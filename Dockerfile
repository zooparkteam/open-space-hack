FROM node:12.19.0-alpine3.10

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV



COPY . /usr/src/app

RUN npm run build-fe

RUN npm install

ENV PORT 8000
EXPOSE $PORT
CMD [ "npm", "start" ]
