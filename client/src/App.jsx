import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import Footer from './components/Footer';

import UserContext from './UserContext';

import Auth from './Auth';
import Devices from './Devices';

import styles from './App.module.css'

const App = () => {
  const [user, dispatch] = useState({
    auth: true,
    admin: true,
    display: 'Maksim Kudryavtsev',
    username: 'maxsior',
    avatar: 'https://lh3.googleusercontent.com/ogw/ADGmqu-heHBYf1NI5-a020tmr-pjvzMSqcUIikAOfyzO0Q=s83-c-mo'
  });

  return (
    <UserContext.Provider value={{user, dispatch}}>
      <div className={styles.app}>
        <Router>
          <Header />

          <div className={styles.content}>
            <Switch>
              <Route exact path="/">
                <h1>Home</h1>
              </Route>
              <Route path="/auth">
                <Auth />
              </Route>
              <Route path="/devices">
                <Devices />
              </Route>
            </Switch>
          </div>

          <Footer />
        </Router>
      </div>
    </UserContext.Provider>
  );
}

export default App;
