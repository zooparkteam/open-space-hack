import React from 'react';
import Button from './components/Button';

import styles from './Auth.module.css';

const Auth = () => {
    return (
      <form className={`container ${styles.auth}`}>
        <h1>Вход</h1>
        <input type='email' name='email' placeholder='example@email.com'/>
        <input type='password' name='password' placeholder='password'/>
        <div className={styles.buttons}>
          <Button ghost type='submit'>Регистрация</Button>
          <Button type='submit'>Вход</Button>
        </div>
        <div className={styles.checkbox}>
          <input type="checkbox" id="input-checkbox" name="checkbox" value="Запомнить логин"/>
          {' '}
          <label htmlFor="input-checkbox">Запомнить логин</label>
        </div>
        <a href="#">Забыли логин или пароль?</a>
      </form>
    );
}

export default Auth;
