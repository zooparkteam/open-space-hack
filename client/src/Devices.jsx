import React from 'react';

import DeviceCard from './components/DeviceCard';
import DeviceCardNew from './components/DeviceCardNew';
import DeviceCardDetail from './components/DeviceCardDetail';

import laptop from './images/Laptop.svg';
import smartphone from './images/Smartphone.svg';
import styles from './Devices.module.css';

let devicesList = [
  {
    "title": "Рабочий ноут",
    "icon": laptop,
  },
  {
    "title": "Мой Lenovo",
    "icon": laptop,
  },
  {
    "title": "Телефон жены",
    "icon": smartphone,
  }
];

const Device = () => {
    const [showDetails, setShowDetails] = React.useState(false)
    const onClick = () => setShowDetails(true)
    return (
      <div className={styles.devices}>
        {devicesList.length > 0 && devicesList.map(x => {
          return <DeviceCard title={x.title} icon={x.icon}/>
        }) }
        <DeviceCardNew onClick={onClick}/>
        { showDetails ? <DeviceCardDetail icon={laptop}/> : null }
      </div>
    );
}

export default Device;
