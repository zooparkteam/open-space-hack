import React, { useContext } from 'react';
import UserContext from './UserContext';

import styles from './User.module.css';

const User = () => {
  const { user } = useContext(UserContext);
  return (
    <div className={styles.user}>
      <span className={styles.display}>{user.display}</span>
      <div className={styles.avatar} style={{
        backgroundImage: `url('${user.avatar}')`
      }} />
    </div>
  );
}

export default User;
