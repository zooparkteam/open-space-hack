import React from 'react';

const UserContext = React.createContext({
  auth: false,
  admin: false,
  display: '',
  username: '',
  avatar: ''
});

export default UserContext;
