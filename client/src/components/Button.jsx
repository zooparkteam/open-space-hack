import React from 'react';

import styles from './Button.module.css'

const Button = ({ small, ghost, text, critical, ...props }) => {
  return (
    <button {...props} className={[
      styles.button,
      small && styles.small,
      ghost && styles.ghost,
      text && styles.text
    ].join(' ')} />
  );
}

export default Button;
