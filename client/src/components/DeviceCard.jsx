import React from 'react';

import trash from '../images/Trash.svg';
import Button from './Button';

const DeviceCard = ({ title, icon }) => {
  return (
    <div className="deviceCard">
      <div className="deviceDelete">
        <Button text>
          <img src={trash} alt="delete" />
        </Button>
      </div>
      <img src={ icon } alt=""/>
      <h2>{title}</h2>
    </div>
  );
}

export default DeviceCard;
