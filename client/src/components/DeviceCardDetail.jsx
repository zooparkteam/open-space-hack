import React from 'react';

import cross from '../images/Cross.svg';
import Button from './Button';

import styles from '../Auth.module.css';

const DeviceCardDetail = ({ icon }) => {
  return (
    <React.Fragment>
      <div className="fade"/>
      <div className="deviceCardDetail">
        
        <div className="deviceDelete">
          <Button text>
            <img src={cross} alt="close" />
          </Button>
        </div>
        <img src={ icon } alt=""/>
        <select name='device_type'>
        <option value="" disabled selected hidden>Тип устройства</option>
          <option>ПК</option>
          <option>Смартфон</option>
        </select>
        <input type='text' name='device_name' placeholder='Название устройства'/>
        <input type='text' name='device_model' placeholder='Модель устройства'/>
        <input type='text' name='os_name' placeholder='Название операционной системы'/>
        <input type='text' name='os_version' placeholder='Версия операционной системы'/>
        <div className={styles.buttons}>
          <Button small ghost type='submit'>Отменить</Button>
          <Button small type='submit'>Сохранить</Button>
        </div>
      </div>
    </React.Fragment>
  );
}

export default DeviceCardDetail;
