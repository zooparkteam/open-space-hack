import React from 'react';

import plus from '../images/Plus.svg';
// import Button from '../components/Button';

const DeviceCardNew = ({ onClick }) => {
  return (
    <div className="deviceCard" onClick={onClick}>
      {/* <Button text> */}
        <img className="center" src={ plus } alt=""/>
      {/* </Button> */}
    </div>
  );
}

export default DeviceCardNew;
