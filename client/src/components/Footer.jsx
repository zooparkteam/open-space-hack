import React from 'react';

import styles from './Footer.module.css';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <p>Built by <a href="https://instagram.com/zoopark.team">@zoopark.team</a> for OPEN SPACE HACK</p>
    </footer>
  );
}

export default Footer;
