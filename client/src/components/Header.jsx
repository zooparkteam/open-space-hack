import React, { useContext } from 'react';

import UserContext from '../UserContext';
import Button from './Button';
import User from '../User';

import logo from '../images/logo-blue-white.png'
import styles from './Header.module.css';

const Header = () => {
  const { user } = useContext(UserContext);

  return (
    <header className={styles.header}>
      <div className={styles.logo}>
        <img src={logo}  alt="open"/>
      </div>
      {user.auth && (
        <div className={styles.buttons}>
          <Button small>Нашёл баг!</Button>
        </div>
      )}
      {user.auth && <User />}
    </header>
  );
}

export default Header;
